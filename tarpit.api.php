<?php

/**
 * @file
 * Hooks for tarpit module.
 */

/**
 * This hook will be triggered when a user is inside a path which is a tarpit.
 */
function hook_tarpit_view() {

}

/**
 * This hook will be triggered when the path has exceeded its limit of maximum
 * segment, the depth.
 */
function hook_tarpit_reaction() {

}

